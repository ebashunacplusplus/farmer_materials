# Документация по оформлению исходных текстов программ

Для форматирования исходников под общий стиль будем использовать утилиту [AStyle](http://astyle.sourceforge.net/).

### Опции astyle
1. Пробел перед условием в **"if","for","while"**.

> Опция: --pad-header

Пример:
```c++
if (isFoo((a+2), b))
    bar(a, b);
```
2. Скобки на одной строке с (оператором,функцией и т.д).

> Опция: --style=google

Пример:
```c++
int Foo(bool isBar) {
    if (isBar) {
        bar();
        return 1;
    } else
        return 0;
}
```
3. Сдвиг **case** внутри **switch**.

> Опция: --indent-switches

Пример:
```c++
switch (foo)
{
    case 1:
        a += 1;
        break;

    case 2:
    {
        a += 2;
        break;
    }
}
```
4. Сдвиг **define** относительно заголовка.

> Опция: --indent-preproc-define

Пример:
```C++
#define Is_Bar(arg,a,b) \
    (Is_Foo ((arg), (a)) \
            || Is_Foo( (arg), (b)))
```
5. Выравнивание указателей(по центру).

> Опция: --align-pointer=middle

Пример:
```C++
char * foo1;
```
6. Выравнивание ссылок(по имени).

> Опция: --align-reference=name

Пример:
```C++
char &foo2;
```
7. Количество оступов(пробелов) должно равняться двум.

> Опция: -s2

Пример:
```C++
int Foo(bool isBar) {
  if (isBar) {
    bar();
    return 1;
  } else
    return 0;
}
```
8. Отступы модификаторов в классе.

> Опция: --indent-classes

Пример:
```C++
class Foo {
    public:
        Foo();
        virtual ~Foo();
};
```
9. Отступы в блоке **namespace**.

> Опция: --indent-namespaces

Пример:
```C++
namespace foospace
{
    class Foo {
        public:
            Foo();
            virtual ~Foo();
    };
}
```
10. Выравнивание для длинных параметров.

> Опция: --indent-after-parens

Пример:
```C++
void Foo(bool bar1,
    bool bar2)
{
    isLongFunction(bar1,
        bar2);

    isLongVariable = foo1
        || foo2;
}
```
11. Отступы для препроцессорного блока.

> Опция: --indent-preproc-block

Пример:
```C++
#ifdef _WIN32
    #include <windows.h>
    #ifndef NO_EXPORT
        #define EXPORT
    #endif
#endif
```
12. Отступ для разных блоков.

> Опция: --break-blocks

Пример:
```C++
isFoo = true;

if (isFoo) {
    bar();
} else {
    anotherBar();
}

isBar = false;
```
13. Пробелы вокруг операторов.

> Опция: --pad-oper 

Пример:
```C++
if (foo == 2)
    a = bar((b - c) * a, d--);
```
14. Пробелы после запятой.

> Опция: --pad-comma

Пример:
```C++
if (isFoo(a, b))
    bar(a, b);
```
15. Если тело в **if** окружено скобками и состоит из строки, то скобки удаляются.

> Опция: --remove-braces

Пример:
```C++
if (isFoo)
    isFoo = false;
```
16. Удаление лишних линий.

> Опция: --delete-empty-lines

Пример:
```C++
void Foo()
{
    foo1 = 1;
    foo2 = 2;
}
```
17. Тело в операторе **if** не должно находиться на одной строке.

> Опция: --break-one-line-headers

Пример:
```C++
void Foo(bool isFoo) {
    if (isFoo1)
        bar1();

    if (isFoo2) {
        bar2();
    }
}
```
18. Закрывает пробел между конечными угловыми скобками шаблона.

> Опция: --close-templates

Пример:
```C++
Stack< int, List< int >> stack1;
```
19. Разрыв строки если она превышает 50 символов.

> Опция: --max-code-length=50

Пример:
```C++
if (thisVariable1 == thatVariable1
        || thisVariable2 == thatVariable2
        || thisVariable3 == thatVariable3)
    bar();
```
20. Логические операции не переходят на новую строку при разрыве.

> Опция: --break-after-logical

Пример:
```C++
if (thisVariable1 == thatVariable1 ||
        thisVariable2 == thatVariable2 ||
        thisVariable3 == thatVariable3)
    bar();
```

# Использование astyle

> astyle [Опции] filepath1 filepath2 [...]

Чтобы привести свои файлы к нашему стилю необходимо ввести:
```sh
user@lol:~$ astyle --style=google --pad-header --indent-switches --indent-preproc-define --align-pointer=middle --align-reference=name -s2 --indent-classes --indent-namespaces --indent-after-parens --indent-preproc-block --break-blocks --pad-oper --pad-comma --remove-braces --delete-empty-lines --break-one-line-headers --close-templates --max-code-length=50 --break-after-logical [ВАШИ ФАЙЛЫ]
```
На выходе вы получите отформатированный файл и ваш оригинальный файл с расширением **.orig** . Если вы не хотите чтобы оставался оригинал, то добавьте **--suffix=none** к существующим опциям.



