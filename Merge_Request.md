#### Инструкция по изменению кода проекта и созданию Merge Request'ов для самых маленьких

1. **Клонирование репозитория**

```
git clone https://gitlab.com/farmerbot/farm-controller-sun.git <PROJECT_DIR>
cd <PROJECT_DIR>
```
2. **Создание своей ветки git**

```
git checkout -b <NAME_BRANCH>
```
**<NAME_BRANCH>** - имя ветки которая начинается с номера задачи которую вы решаете в этой ветке. Например у вас есть задача в GitLab #11, тогда ваша команда для создания ветки будет выглядеть вот так: `git checkout -b Issue_#11`

3. **Внесение изменений в код**

*Тут мы решаем поставленную задачу*

4. **Фиксирование изменений**

* Если мы введем команду `git status` то можем обнаружить какие файлы притерпели изменения
```
git status
На ветке Issue_#11
Изменения, которые не в индексе для коммита:
(используйте «git add <файл>…», чтобы добавить файл в индекс)
(используйте «git checkout — <файл>…», чтобы отменить изменения
в рабочем каталоге)

изменено: main.c

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
```
* Нужно добавить измененные файлы в индекс.
Сделать это можно командой `git add <file>`
```
git add <file>
```
* Теперь нужно создать **commit** и сделать **push** наших изменений на сервер
```
git commit -m "<comment>"
git push origin <NAME_BRANCH>
```
5. **Создание Merge Request**

Теперь ваша ветка на сервере.
Необходимо создать **merge request** чтобы Assignee сделал **Code Review** и слил ваш код в **target** ветку.

* New merge request

![1.bmp](https://gitlab.com/ebashunacplusplus/farmer_materials/raw/master/1.png)

* Выбираем ту ветку которую мы хотим слить и ту ветку в которую мы хотим сливать. В нашем случае мы сливаем нашу **Issue_#11** в **master**

![2.bmp](https://gitlab.com/ebashunacplusplus/farmer_materials/raw/master/2.png)
Жмем большую зеленую кнопку.

* **Title** - описание задачи, сюда вы пишите что вы исправили

Описание должно быть в виде **Issue_#<номер задачи>: <описание>**

![3.bmp](https://gitlab.com/ebashunacplusplus/farmer_materials/raw/master/3.png)

Метка WIP ставится тогда когда вы еще работаете над задачей, чтобы ваш код небыл слит по ошибке.

* **Assignee** - тот кто будет проводить Code Review

![4.bmp](https://gitlab.com/ebashunacplusplus/farmer_materials/raw/master/4.png)

* Ставим галку **Delete source branc**, чтобы ваша ветка удалилась после **Merge**

![](https://gitlab.com/ebashunacplusplus/farmer_materials/raw/master/5.png)

Жмем **Submit merge request** и ждем правки
